gluten-dump
===========

gluten-dump is basically a disassembler for gluten bytecode, which is used by
the [Seitan project](https://seitan.rocks/) to prescribe how to execute an
intercepted syscall.

```
$ gluten-dump connect.gluten
[0x0]: nr rodata[0x0]<u64>=0x2a; no match: instruction[0xa]<()>
[0x1]: load (size=0x80) seccompdata[0x1]<()> -> data[0x0]<()>
[0x2]: cmp (size=0x2) data[0x0]<u16>=0x0 != rodata[0x8]<u16>=0x1; jump to: instruction[0xa]<()>
[0x3]: cmp (size=0xb) data[0x2]<()>="\0\0\0\0\0\0\0\0\0\0\0" != rodata[0x2a]<()>="/cool.sock\0"; jump to: instruction[0xa]<()>
[0x4]: call 41 arg[0]=rodata[0x55]<i64>=0x1 arg[1]=data[0x80]<i64>=0x1 arg[2]=rodata[0x5d]<i64>=0x0 -> data[0x84]<i64>
[0x5]: copy (size=0x4) data[0x84]<()> -> data[0x8c]<()>
[0x6]: call 42 arg[0]=data[0x8c]<i64>=0x0 arg[1]=&rodata[0x7d]<()> arg[2]=rodata[0xfd]<i64>=0x6e -> data[0x90]<i64>
[0x7]: fd seccompdata[0x0]<i32> -> data[0x84]<i32>=0x0; setfd !cloexec !do_return
[0x8]: return rodata[0x12d]<i64>=0x0; error=rodata[0x129]<i32>=0x0; !cont
[0x9]: end
[0xa]: nr rodata[0x141]<u64>=0x2a; no match: instruction[0x10]<()>
[0xb]: load (size=0x80) seccompdata[0x1]<()> -> data[0x98]<()>
[0xc]: cmp (size=0x2) data[0x98]<u16>=0x0 != rodata[0x149]<u16>=0x1; jump to: instruction[0x10]<()>
[0xd]: cmp (size=0xc) data[0x9a]<()>="\0\0\0\0\0\0\0\0\0\0\0\0" != rodata[0x16b]<()>="/error.sock\0"; jump to: instruction[0x10]<()>
[0xe]: return rodata[0x19b]<i64>=0x0; error=rodata[0x197]<i32>=0xffffffff; !cont
[0xf]: end
[0x10]: nr rodata[0x1af]<u64>=0x2a; no match: instruction[0x13]<()>
[0x11]: return null<i64>; error=null<i32>; cont
[0x12]: end
```
