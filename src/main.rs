#![deny(unsafe_op_in_unsafe_fn)]

mod as_bytes;
mod compat;
mod error_context;
pub mod gluten;
mod macros;

use compat::io_error_other;
use std::io;
use std::{env, fs};

fn main() -> io::Result<()> {
    let mut args = env::args().skip(1);
    let filename = args
        .next()
        .ok_or_else(|| io_error_other("No filename given"))?;

    let file = fs::OpenOptions::new().read(true).open(filename)?;
    let gluten = gluten::File::read(file)?;

    for (i, insn) in gluten.printable_instructions_iter(true).enumerate() {
        println!("[{i:#x}]: {insn}");
    }

    Ok(())
}
