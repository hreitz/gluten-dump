// Functions that are present in newer or unstable Rust versions, but not yet in versions that are
// still quite widely in use

use std::error::Error;
use std::io;
use std::mem::MaybeUninit;
use std::ops::{Index, RangeFrom, RangeTo};

/// `io::Error::other()`: Feature io_error_other
pub fn io_error_other<E: Into<Box<dyn Error + Send + Sync>>>(e: E) -> io::Error {
    io::Error::new(io::ErrorKind::Other, e)
}

/// `&[T].split_once()`: Feature slice_split_once
pub trait SliceSplitOnceCompat {
    type Item;
    type Head;
    type Tail;

    fn split_once_compat<F: FnMut(Self::Item) -> bool>(
        self,
        pred: F,
    ) -> Option<(Self::Head, Self::Tail)>;
}

impl<'a, T> SliceSplitOnceCompat for &'a T
where
    T: Index<RangeFrom<usize>>,
    T: Index<RangeTo<usize>>,
    &'a T: IntoIterator,
{
    type Item = <&'a T as IntoIterator>::Item;
    type Head = &'a <T as Index<RangeTo<usize>>>::Output;
    type Tail = &'a <T as Index<RangeFrom<usize>>>::Output;

    fn split_once_compat<F: FnMut(Self::Item) -> bool>(
        self,
        pred: F,
    ) -> Option<(Self::Head, Self::Tail)> {
        let index = self.into_iter().position(pred)?;
        Some((&self[..index], &self[index + 1..]))
    }
}

/// `Box::new_uninit()` / `Box::assume_init()`: Feature new_uninit
pub trait NewUninitCompat {
    type WithoutUninit;

    fn new_uninit_compat() -> Self;
    unsafe fn assume_init_compat(self) -> Self::WithoutUninit;
}

impl<T> NewUninitCompat for Box<MaybeUninit<T>> {
    type WithoutUninit = Box<T>;

    fn new_uninit_compat() -> Self {
        let layout = std::alloc::Layout::new::<MaybeUninit<T>>();
        let ptr = unsafe { std::alloc::alloc(layout) };
        unsafe { Box::from_raw(ptr.cast()) }
    }

    unsafe fn assume_init_compat(self) -> Box<T> {
        let raw = Box::into_raw(self);
        unsafe { Box::from_raw(raw.cast()) }
    }
}
