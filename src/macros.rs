#[macro_export]
macro_rules! numerical_enum {
    (
        $(#[$attr:meta])*
        pub enum $enum_name:ident as $repr:tt/$conv_type:tt {
            $(
                $(#[$id_attr:meta])*
                $identifier:ident = $value:literal,
            )+
        }
    ) => {
        $(#[$attr])*
        #[repr($repr)]
        pub enum $enum_name {
            $(
                $(#[$id_attr])*
                $identifier = $value,
            )+
        }

        impl TryFrom<$conv_type> for $enum_name {
            type Error = std::io::Error;
            fn try_from(val: $conv_type) -> std::io::Result<Self> {
                match val {
                    $($value => Ok($enum_name::$identifier),)*
                    _ => Err($crate::compat::io_error_other(format!(
                        "Invalid value for {}: {:x}",
                        stringify!($enum_name),
                        val
                    ))),
                }
            }
        }
    }
}
