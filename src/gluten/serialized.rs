use crate::numerical_enum;
use libc::c_int;
use std::fmt;
use std::io;
use std::mem::size_of;

pub mod op;

#[repr(C, packed)]
pub struct File {
    pub header: Header,
    pub inst: [u8; 1 << 20],
    pub ro_data: [u8; 1 << 20],
    pub metadata: c_int,
    pub data: [u8; 1 << 20],
}

#[repr(C)]
pub struct Header {
    pub relocation: [Offset; 256],
}

pub struct InstructionIterator<'a> {
    buffer: Option<&'a [u8]>,
}

numerical_enum! {
    #[derive(Copy, Clone, Debug, Eq, PartialEq)]
    pub enum MetadataType as C/c_int {
        UidTarget = 0,
        GidTarget = 1,
        PidTarget = 2,
    }
}

/// Bit field of type and offset (for whatever reason), could actually have any size, but it is
/// `u32` right now.
#[repr(C, packed)]
pub struct Offset(u32);

numerical_enum! {
    #[derive(Copy, Clone, Debug, Default, Eq, PartialEq)]
    pub enum OffsetType as u32/u32 {
        #[default]
        Null = 0,
        RoData = 1,
        Data = 2,
        SeccompData = 3,
        Instruction = 4,
        Metadata = 5,
    }
}

impl File {
    pub fn metadata(&self) -> io::Result<MetadataType> {
        self.metadata.try_into()
    }
}

impl<'a> InstructionIterator<'a> {
    pub fn new(buffer: &'a [u8]) -> Self {
        InstructionIterator {
            buffer: Some(buffer),
        }
    }
}

impl<'a> Iterator for InstructionIterator<'a> {
    type Item = &'a op::Op;

    fn next(&mut self) -> Option<Self::Item> {
        let buffer = self.buffer.take()?;
        let (op, tail) = buffer.split_at(size_of::<op::Op>());
        // Not really safe; it is what it is.
        let op = unsafe { &*(op.as_ptr().cast::<op::Op>()) };
        if !tail.is_empty() {
            self.buffer = Some(tail);
        }
        Some(op)
    }
}

impl Offset {
    pub fn typ(&self) -> io::Result<OffsetType> {
        // Implementation-defined by compiler (should probably not be a bit field), but this is
        // what is de-facto.
        (self.0 & 0x7).try_into()
    }

    pub fn offset(&self) -> u32 {
        // See `typ()`
        self.0 >> 3
    }
}

// Manually implement what `#[derive(Debug)]` would do automatically.  In older Rust versions,
// `self.0` was considered unaligned regardless of context, so `#[derive(Debug)]` throws a warning
// (but not in newer versions).
impl fmt::Debug for Offset {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let ofs = self.0;
        write!(f, "{ofs}")
    }
}
