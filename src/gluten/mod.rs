pub mod serialized;

use crate::as_bytes::ReadObject;
use crate::compat::{io_error_other, SliceSplitOnceCompat};
use crate::error_context::ResultErrorContext;
use libc::{c_int, c_long};
use std::fmt::{self, Debug, Display};
use std::io::{self, Read};
use std::mem::size_of;

pub struct File {
    serialized_file: Box<serialized::File>,
    relocation: Vec<Offset>,
    instructions: Vec<io::Result<Instruction>>,
}

#[derive(Clone, Debug)]
pub enum Offset {
    Null,
    RoData(usize),
    Data(usize),
    SeccompData(usize),
    Instruction(usize),
    Metadata(MetadataType),
}

pub struct Reference<T: Sized> {
    offset: Offset,
    _phantom: std::marker::PhantomData<T>,
}

pub struct PrintableReference<'a, 'b, T: Sized> {
    reference: &'a Reference<T>,
    file: &'b serialized::File,
}

#[derive(Clone, Debug)]
pub enum MetadataType {
    UidTarget,
    GidTarget,
    PidTarget,
}

#[derive(Debug)]
pub enum Instruction {
    End,
    Nr {
        nr: Reference<u64>,
        no_match: Reference<()>,
    },
    Call {
        nr: u32,
        context: Vec<Context>,
        args: Vec<CallArg>,
        ret: Option<Reference<c_long>>,
    },
    Copy {
        src: Reference<()>,
        dst: Reference<()>,
        size: usize,
    },
    Fd {
        srcfd: Reference<c_int>,
        newfd: Reference<c_int>,
        setfd: bool,
        cloexec: bool,
        do_return: bool,
    },
    FdGet {
        src: Reference<c_int>,
        dst: Reference<c_int>,
    },
    Return {
        val: Reference<c_long>,
        error: Reference<c_int>,
        cont: bool,
    },
    Load {
        src: Reference<()>,
        dst: Reference<()>,
        size: usize,
    },
    Store {
        src: Reference<()>,
        dst: Reference<()>,
        count: Reference<()>,
    },
    Bitwise {
        size: usize,
        typ: BitwiseType,
        dst: Reference<()>,
        x: Reference<()>,
        y: Reference<()>,
    },
    IovLoad {
        iov: Reference<()>,
        iovlen: Reference<usize>,
        dst: Reference<()>,
        size: usize,
    },
    Cmp {
        cmp: CmpType,
        size: usize,
        x: Reference<()>,
        y: Reference<()>,
        jmp: Reference<()>,
    },
    ResolveFd {
        fd: Reference<c_int>,
        path: Reference<()>,
        typ: ResolveFdType,
        path_max: usize,
    },
}

pub struct PrintableInstruction<'a, 'b> {
    instruction: &'a io::Result<Instruction>,
    file: &'b serialized::File,
}

pub struct PrintableInstructionsIter<'a, 'b, I: Iterator<Item = &'a io::Result<Instruction>>> {
    insn_iter: I,
    file: &'b serialized::File,
    skip_multi_end: bool,
    skipping_end: bool,
}

#[derive(Debug)]
pub enum Context {
    NsMount(ContextString),
    NsCgroup(ContextString),
    NsUts(ContextString),
    NsIpc(ContextString),
    NsUser(ContextString),
    NsPid(ContextString),
    NsNet(ContextString),
    NsTime(ContextString),
    Cwd(ContextString),
    Uid(ContextId),
    Gid(ContextId),
}

#[derive(Debug)]
pub enum ContextString {
    OfCaller,
    OfPid(u32),
    Fixed(String),
}

#[derive(Debug)]
pub enum ContextId {
    OfCaller,
    Numeric(u32),
    Name(String),
}

#[derive(Debug)]
pub enum CallArg {
    Immediate(Reference<c_long>),
    Pointer(Reference<()>),
}

#[derive(Debug)]
pub enum ContextTarget {
    NsMount(String),
    NsCgroup(String),
    NsUts(String),
    NsIpc(String),
    NsUser(String),
    NsPid(u32),
    NsNet(String),
    NsTime(String),
    Cwd(String),
    Uid(u32),
    Gid(u32),
}

#[derive(Debug)]
pub enum ContextSpecType {
    None,
    Caller,
    Num,
    Name,
}

#[derive(Debug)]
pub enum BitwiseType {
    And,
    Or,
}

#[derive(Debug)]
pub enum CmpType {
    Eq,
    Ne,
    Gt,
    Ge,
    Lt,
    Le,
}

#[derive(Debug)]
pub enum ResolveFdType {
    Path,
    Mount,
}

impl File {
    pub fn read<R: Read>(mut stream: R) -> io::Result<Self> {
        // Safe: Everything will be checked for validity
        let ser_file: Box<serialized::File> = unsafe { stream.read_boxed_object() }?;

        let relocation = ser_file
            .header
            .relocation
            .iter()
            .map(TryFrom::try_from)
            .collect::<io::Result<Vec<Offset>>>()?;

        let insn_it = serialized::InstructionIterator::new(&ser_file.inst);
        let instructions = insn_it
            .map(|op| Instruction::fetch(op, &ser_file))
            .collect();

        Ok(File {
            serialized_file: ser_file,
            relocation,
            instructions,
        })
    }

    pub fn relocation(&self) -> &[Offset] {
        &self.relocation
    }

    pub fn instructions(&self) -> &[io::Result<Instruction>] {
        &self.instructions
    }

    pub fn printable_instructions_iter(
        &self,
        skip_multi_end: bool,
    ) -> PrintableInstructionsIter<'_, '_, std::slice::Iter<'_, io::Result<Instruction>>> {
        PrintableInstructionsIter {
            insn_iter: self.instructions.iter(),
            file: &self.serialized_file,
            skip_multi_end,
            skipping_end: false,
        }
    }
}

impl Offset {
    pub fn is_null(&self) -> bool {
        matches!(self, Offset::Null)
    }

    pub fn fetch_const<'a, T: Sized>(&self, file: &'a serialized::File) -> io::Result<&'a T> {
        match self {
            Offset::Null => Err(io_error_other("null reference")),
            Offset::RoData(_) => self.fetch_any(file),
            Offset::Data(_) => Err(io_error_other("mutable data reference")),
            Offset::SeccompData(_) => Err(io_error_other("mutable seccomp data reference")),
            Offset::Instruction(_) => self.fetch_any(file),
            Offset::Metadata(typ) => Err(io_error_other(format!("metadata reference to {typ:?}"))),
        }
    }

    pub fn fetch_any<'a, T: Sized>(&self, file: &'a serialized::File) -> io::Result<&'a T> {
        let sz = size_of::<T>();
        let slice = self.fetch_slice(file, sz)?;
        assert!(slice.len() == sz);
        // Not really safe; it is what it is.  At least the size matches.
        Ok(unsafe { &*(slice.as_ptr().cast::<T>()) })
    }

    pub fn fetch_slice<'a>(&self, file: &'a serialized::File, len: usize) -> io::Result<&'a [u8]> {
        match self {
            Offset::Null => Err(io_error_other("null reference")),

            Offset::RoData(ofs) => {
                let end = ofs.checked_add(len).ok_or_else(|| {
                    io_error_other(format!("Reference {ofs:#x} + {len:#x} overflow"))
                })?;
                file.ro_data.get(*ofs..end).ok_or_else(|| {
                    io_error_other(format!("RoData reference {ofs:#x}..{end:#x} out of bounds"))
                })
            }

            Offset::Data(ofs) => {
                let end = ofs.checked_add(len).ok_or_else(|| {
                    io_error_other(format!("Reference {ofs:#x} + {len:#x} overflow"))
                })?;
                file.data.get(*ofs..end).ok_or_else(|| {
                    io_error_other(format!("Data reference {ofs:#x}..{end:#x} out of bounds"))
                })
            }

            Offset::SeccompData(_) => Err(io_error_other("seccomp data reference")),

            Offset::Instruction(ofs) => {
                let end = ofs.checked_add(len).ok_or_else(|| {
                    io_error_other(format!(
                        "Instruction reference {ofs:#x}..{len:#x} out of bounds"
                    ))
                })?;
                file.inst.get(*ofs..end).ok_or_else(|| {
                    io_error_other(format!(
                        "Instruction reference {ofs:#x}..{end:#x} out of bounds"
                    ))
                })
            }

            Offset::Metadata(typ) => Err(io_error_other(format!("metadata reference to {typ:?}"))),
        }
    }

    pub fn offset(&self, ofs: usize) -> io::Result<Self> {
        match self {
            Offset::Null => Err(io_error_other("cannot add offset to null reference")),

            Offset::RoData(base) => base
                .checked_add(ofs)
                .ok_or_else(|| io_error_other("offset overflow ({base:#x} + {ofs:#x})"))
                .map(Offset::RoData),

            Offset::Data(base) => base
                .checked_add(ofs)
                .ok_or_else(|| io_error_other("offset overflow ({base:#x} + {ofs:#x})"))
                .map(Offset::Data),

            Offset::SeccompData(base) => base
                .checked_add(ofs)
                .ok_or_else(|| io_error_other("offset overflow ({base:#x} + {ofs:#x})"))
                .map(Offset::SeccompData),

            Offset::Instruction(base) => base
                .checked_add(ofs)
                .ok_or_else(|| io_error_other("offset overflow ({base:#x} + {ofs:#x})"))
                .map(Offset::Instruction),

            Offset::Metadata(_) => Err(io_error_other("Cannot add offset to metadata reference")),
        }
    }
}

impl Display for Offset {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Offset::Null => write!(f, "null"),
            Offset::RoData(ofs) => write!(f, "rodata[{ofs:#x}]"),
            Offset::Data(ofs) => write!(f, "data[{ofs:#x}]"),
            Offset::SeccompData(ofs) => write!(f, "seccompdata[{ofs:#x}]"),
            Offset::Instruction(ofs) => write!(f, "instruction[{ofs:#x}]"),
            Offset::Metadata(t) => write!(f, "metadata[{t}]"),
        }
    }
}

impl TryFrom<&serialized::Offset> for Offset {
    type Error = io::Error;

    fn try_from(serialized: &serialized::Offset) -> io::Result<Self> {
        match serialized.typ()? {
            serialized::OffsetType::Null => Ok(Offset::Null),
            serialized::OffsetType::RoData => Ok(Offset::RoData(serialized.offset() as usize)),
            serialized::OffsetType::Data => Ok(Offset::Data(serialized.offset() as usize)),
            serialized::OffsetType::SeccompData => {
                Ok(Offset::SeccompData(serialized.offset() as usize))
            }
            serialized::OffsetType::Instruction => {
                Ok(Offset::Instruction(serialized.offset() as usize))
            }
            serialized::OffsetType::Metadata => {
                let mt: c_int = serialized.offset().try_into().map_err(io_error_other)?;
                let mt: serialized::MetadataType = mt.try_into()?;
                Ok(Offset::Metadata((&mt).into()))
            }
        }
    }
}

impl Reference<()> {
    fn with_type<T: Sized>(&self) -> Reference<T> {
        Reference {
            offset: self.offset.clone(),
            _phantom: std::marker::PhantomData,
        }
    }
}

impl<T: Debug + Sized> Reference<T> {
    fn printable<'a, 'b>(&'a self, file: &'b serialized::File) -> PrintableReference<'a, 'b, T> {
        PrintableReference {
            reference: self,
            file,
        }
    }
}

impl<T: Sized> Debug for Reference<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{:?}<{}>", self.offset, std::any::type_name::<T>())
    }
}

impl<T: Sized> Display for Reference<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}<{}>", self.offset, std::any::type_name::<T>())
    }
}

impl<T: Sized> Debug for PrintableReference<'_, '_, T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{:?}", self.reference)
    }
}

impl<T: Debug + Sized> Display for PrintableReference<'_, '_, T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.reference)?;
        if size_of::<T>() > 0 {
            // Just ignore errors
            if let Ok(obj) = self.reference.offset.fetch_any::<T>(self.file) {
                write!(f, "={obj:#x?}")?;
            }
        }
        Ok(())
    }
}

impl<T> TryFrom<&serialized::Offset> for Reference<T> {
    type Error = io::Error;

    fn try_from(serialized: &serialized::Offset) -> io::Result<Self> {
        Ok(Reference {
            offset: serialized.try_into()?,
            _phantom: std::marker::PhantomData,
        })
    }
}

impl Instruction {
    fn fetch(op: &serialized::op::Op, file: &serialized::File) -> io::Result<Self> {
        let op_type: serialized::op::Type = op.typ().err_context(|| "Decoding op type")?;

        match op_type {
            serialized::op::Type::End => Self::fetch_end().err_context(|| "end"),

            serialized::op::Type::Nr => {
                // Safe: Checked type
                Self::fetch_nr(unsafe { &op.op.nr }).err_context(|| "nr")
            }

            serialized::op::Type::Call => {
                // Safe: Checked type
                let p = unsafe { &op.op.call };
                let desc_ofs: Offset = (&p.desc).try_into().err_context(|| "call desc")?;
                let desc = desc_ofs.fetch_const(file).err_context(|| "call desc")?;
                let arg_desc_ofs = desc_ofs
                    .offset(size_of::<serialized::op::desc::Syscall>())
                    .err_context(|| "Call args vector")?;
                Self::fetch_call(desc, arg_desc_ofs, file).err_context(|| "call")
            }

            serialized::op::Type::Copy => {
                // Safe: Checked type
                Self::fetch_copy(unsafe { &op.op.copy }).err_context(|| "copy")
            }

            serialized::op::Type::Fd => {
                // Safe: Checked type
                let p = unsafe { &op.op.fd };
                let desc = (&p.desc)
                    .try_into()
                    .and_then(|o: Offset| o.fetch_const(file))
                    .err_context(|| "fd desc")?;
                Self::fetch_fd(desc).err_context(|| "fd")
            }

            serialized::op::Type::FdGet => {
                // Safe: Checked type
                Self::fetch_fdget(unsafe { &op.op.fdget }).err_context(|| "fdget")
            }

            serialized::op::Type::Return => {
                // Safe: Checked type
                let p = unsafe { &op.op.ret };
                let desc = (&p.desc)
                    .try_into()
                    .and_then(|o: Offset| o.fetch_const(file))
                    .err_context(|| "return desc")?;
                Self::fetch_return(desc).err_context(|| "return")
            }

            serialized::op::Type::Load => {
                // Safe: Checked type
                Self::fetch_load(unsafe { &op.op.load }).err_context(|| "load")
            }

            serialized::op::Type::Store => {
                // Safe: Checked type
                Self::fetch_store(unsafe { &op.op.store }).err_context(|| "store")
            }

            serialized::op::Type::IovLoad => {
                // Safe: Checked type
                Self::fetch_iov_load(unsafe { &op.op.iovload }).err_context(|| "iovload")
            }

            serialized::op::Type::Bitwise => {
                // Safe: Checked type
                let p = unsafe { &op.op.bitwise };
                let desc = (&p.desc)
                    .try_into()
                    .and_then(|o: Offset| o.fetch_const(file))
                    .err_context(|| "bitwise desc")?;
                Self::fetch_bitwise(desc).err_context(|| "bitwise")
            }

            serialized::op::Type::Cmp => {
                // Safe: Checked type
                let p = unsafe { &op.op.cmp };
                let desc = (&p.desc)
                    .try_into()
                    .and_then(|o: Offset| o.fetch_const(file))
                    .err_context(|| "cmp desc")?;
                Self::fetch_cmp(desc).err_context(|| "cmp")
            }

            serialized::op::Type::ResolveFd => {
                // Safe: Checked type
                let p = unsafe { &op.op.resfd };
                let desc = (&p.desc)
                    .try_into()
                    .and_then(|o: Offset| o.fetch_const(file))
                    .err_context(|| "resolvefd desc")?;
                Self::fetch_resolvefd(desc).err_context(|| "resolvefd")
            }
        }
    }

    fn fetch_end() -> io::Result<Self> {
        Ok(Instruction::End)
    }

    fn fetch_nr(p: &serialized::op::Nr) -> io::Result<Self> {
        Ok(Instruction::Nr {
            nr: (&p.nr).try_into().err_context(|| "nr")?,
            no_match: (&p.no_match).try_into().err_context(|| "no_match")?,
        })
    }

    fn fetch_call(
        p: &serialized::op::desc::Syscall,
        mut args: Offset,
        file: &serialized::File,
    ) -> io::Result<Self> {
        p.bitfield.check_valid()?;

        let mut context_ofs: Offset = (&p.context).try_into().err_context(|| "context")?;
        let mut contexts = Vec::new();
        if !context_ofs.is_null() {
            loop {
                let context: &serialized::op::desc::Context = context_ofs
                    .fetch_const(file)
                    .err_context(|| format!("context[{}]", contexts.len()))?;
                let spec = context
                    .type_spec
                    .spec()
                    .err_context(|| format!("context[{}]: spec", contexts.len()))?;
                // For some reason, an erroneous type is supposed to work as the end marker, too
                if matches!(spec, serialized::op::desc::ContextSpec::None)
                    || context.type_spec.typ().is_err()
                {
                    break;
                }

                let context = context
                    .try_into()
                    .err_context(|| format!("context[{}]", contexts.len()))?;
                contexts.push(context);

                context_ofs = context_ofs
                    .offset(size_of::<serialized::op::desc::Context>())
                    .err_context(|| "context vector")?;
            }
        }

        let arg_deref = p.bitfield.arg_deref();
        let has_ret = p.bitfield.has_ret();
        let mut arg_vec = Vec::new();
        for i in 0..p.bitfield.arg_count() {
            let location: Reference<()> = args
                .fetch_const::<serialized::Offset>(file)
                .and_then(TryInto::try_into)
                .err_context(|| format!("arg {i}"))?;

            let call_arg = if arg_deref & (1 << i) != 0 {
                CallArg::Pointer(location)
            } else {
                CallArg::Immediate(location.with_type())
            };
            arg_vec.push(call_arg);

            if i < p.bitfield.arg_count() || has_ret {
                args = args
                    .offset(size_of::<serialized::Offset>())
                    .err_context(|| "args vector")?;
            }
        }
        let ret = if has_ret {
            Some(
                args.fetch_const::<serialized::Offset>(file)
                    .and_then(TryInto::try_into)
                    .err_context(|| "ret")?,
            )
        } else {
            None
        };

        Ok(Instruction::Call {
            nr: p.bitfield.nr(),
            context: contexts,
            args: arg_vec,
            ret,
        })
    }

    fn fetch_copy(p: &serialized::op::Copy) -> io::Result<Self> {
        Ok(Instruction::Copy {
            src: (&p.src).try_into().err_context(|| "src")?,
            dst: (&p.dst).try_into().err_context(|| "dst")?,
            size: p.size,
        })
    }

    fn fetch_fd(p: &serialized::op::desc::Fd) -> io::Result<Self> {
        p.flags.check_valid().err_context(|| "flags")?;
        Ok(Instruction::Fd {
            srcfd: (&p.srcfd).try_into().err_context(|| "srcfd")?,
            newfd: (&p.newfd).try_into().err_context(|| "newfd")?,
            setfd: p.flags.setfd(),
            cloexec: p.flags.cloexec(),
            do_return: p.flags.do_return(),
        })
    }

    fn fetch_fdget(p: &serialized::op::FdGet) -> io::Result<Self> {
        Ok(Instruction::FdGet {
            src: (&p.src).try_into().err_context(|| "src")?,
            dst: (&p.dst).try_into().err_context(|| "dst")?,
        })
    }

    fn fetch_return(p: &serialized::op::desc::Return) -> io::Result<Self> {
        Ok(Instruction::Return {
            val: (&p.val).try_into().err_context(|| "val")?,
            error: (&p.error).try_into().err_context(|| "error")?,
            cont: p.cont,
        })
    }

    fn fetch_load(p: &serialized::op::Load) -> io::Result<Self> {
        Ok(Instruction::Load {
            src: (&p.src).try_into().err_context(|| "src")?,
            dst: (&p.dst).try_into().err_context(|| "dst")?,
            size: p.size,
        })
    }

    fn fetch_store(p: &serialized::op::Store) -> io::Result<Self> {
        Ok(Instruction::Store {
            src: (&p.src).try_into().err_context(|| "src")?,
            dst: (&p.dst).try_into().err_context(|| "dst")?,
            count: (&p.count).try_into().err_context(|| "count")?,
        })
    }

    fn fetch_bitwise(p: &serialized::op::desc::Bitwise) -> io::Result<Self> {
        Ok(Instruction::Bitwise {
            size: p.size,
            typ: (&p.typ().err_context(|| "type")?).into(),
            dst: (&p.dst).try_into().err_context(|| "dst")?,
            x: (&p.x).try_into().err_context(|| "x")?,
            y: (&p.y).try_into().err_context(|| "y")?,
        })
    }

    fn fetch_iov_load(p: &serialized::op::IovLoad) -> io::Result<Self> {
        Ok(Instruction::IovLoad {
            iov: (&p.iov).try_into().err_context(|| "iov")?,
            iovlen: (&p.iovlen).try_into().err_context(|| "iovlen")?,
            dst: (&p.dst).try_into().err_context(|| "dst")?,
            size: p.size,
        })
    }

    fn fetch_cmp(p: &serialized::op::desc::Cmp) -> io::Result<Self> {
        Ok(Instruction::Cmp {
            cmp: (&p.cmp().err_context(|| "cmp")?).into(),
            size: p.size,
            x: (&p.x).try_into().err_context(|| "x")?,
            y: (&p.y).try_into().err_context(|| "y")?,
            jmp: (&p.jmp).try_into().err_context(|| "jmp")?,
        })
    }

    fn fetch_resolvefd(p: &serialized::op::desc::ResolveFd) -> io::Result<Self> {
        Ok(Instruction::ResolveFd {
            fd: (&p.fd).try_into().err_context(|| "fd")?,
            path: (&p.path).try_into().err_context(|| "path")?,
            typ: (&p.typ().err_context(|| "type")?).into(),
            path_max: p.path_max,
        })
    }
}

impl Display for PrintableInstruction<'_, '_> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let instruction = match self.instruction {
            Ok(insn) => insn,
            Err(err) => return write!(f, "ERROR: {err}"),
        };

        match instruction {
            Instruction::End => write!(f, "end"),

            Instruction::Nr { nr, no_match } => {
                let nr = nr.printable(self.file);
                let no_match = no_match.printable(self.file);
                write!(f, "nr {nr}; no match: {no_match}")
            }

            Instruction::Call {
                nr,
                context,
                args,
                ret,
            } => {
                write!(f, "call {nr}")?;
                for (i, ctx) in context.iter().enumerate() {
                    write!(f, " context[{i}]:{ctx}")?;
                }
                for (i, arg) in args.iter().enumerate() {
                    match arg {
                        CallArg::Immediate(arg) => {
                            let arg = arg.printable(self.file);
                            write!(f, " arg[{i}]={arg}")?;
                        }
                        CallArg::Pointer(loc) => {
                            let loc = loc.printable(self.file);
                            write!(f, " arg[{i}]=&{loc}")?;
                        }
                    }
                }
                if let Some(ret) = ret {
                    // Do not run `printable`; this is write-only, no need to try to fetch anything
                    write!(f, " -> {ret}")?;
                }
                Ok(())
            }

            Instruction::Copy { src, dst, size } => {
                let src = src.printable(self.file);
                let dst = dst.printable(self.file);
                write!(f, "copy (size={size:#x}) {src} -> {dst}")
            }

            Instruction::Fd {
                srcfd,
                newfd,
                setfd,
                cloexec,
                do_return,
            } => {
                let srcfd = srcfd.printable(self.file);
                let newfd = newfd.printable(self.file);
                write!(
                    f,
                    "fd {} -> {}; {}setfd {}cloexec {}do_return",
                    srcfd,
                    newfd,
                    if *setfd { "" } else { "!" },
                    if *cloexec { "" } else { "!" },
                    if *do_return { "" } else { "!" },
                )
            }

            Instruction::FdGet { src, dst } => {
                let src = src.printable(self.file);
                let dst = dst.printable(self.file);
                write!(f, "fdget {src} -> {dst}")
            }

            Instruction::Return { val, error, cont } => {
                let val = val.printable(self.file);
                let error = error.printable(self.file);
                write!(
                    f,
                    "return {}; error={}; {}cont",
                    val,
                    error,
                    if *cont { "" } else { "!" },
                )
            }

            Instruction::Load { src, dst, size } => {
                let src = src.printable(self.file);
                let dst = dst.printable(self.file);
                write!(f, "load (size={size:#x}) {src} -> {dst}")
            }

            Instruction::Store { src, dst, count } => {
                let src = src.printable(self.file);
                let dst = dst.printable(self.file);
                let count = count.printable(self.file);
                write!(f, "store {src} -> {dst}; count={count}")
            }

            Instruction::Bitwise {
                size,
                typ,
                dst,
                x,
                y,
            } => {
                fn fmt_bitwise<T: Debug + Sized>(
                    f: &mut fmt::Formatter,
                    file: &serialized::File,
                    size: &usize,
                    typ: &BitwiseType,
                    dst: &Reference<()>,
                    x: &Reference<()>,
                    y: &Reference<()>,
                ) -> fmt::Result {
                    let x = x.with_type::<T>();
                    let y = y.with_type::<T>();

                    let x = x.printable(file);
                    let y = y.printable(file);
                    // Do not run `printable` on `dst`; it is write-only, no need to try to fetch anything

                    write!(f, "bitwise (size={size:#x}) {x} {typ} {y} -> {dst}")
                }

                match size {
                    1 => fmt_bitwise::<u8>(f, self.file, size, typ, dst, x, y),
                    2 => fmt_bitwise::<u16>(f, self.file, size, typ, dst, x, y),
                    4 => fmt_bitwise::<u32>(f, self.file, size, typ, dst, x, y),
                    8 => fmt_bitwise::<u64>(f, self.file, size, typ, dst, x, y),
                    _ => fmt_bitwise::<()>(f, self.file, size, typ, dst, x, y),
                }
            }

            Instruction::IovLoad {
                iov,
                iovlen,
                dst,
                size,
            } => {
                let iov = iov.printable(self.file);
                let iovlen = iovlen.printable(self.file);
                let dst = dst.printable(self.file);
                write!(
                    f,
                    "iovload (size={size:#x}) iov={iov} iovlen={iovlen} -> {dst}"
                )
            }

            Instruction::Cmp {
                cmp,
                size,
                x,
                y,
                jmp,
            } => {
                fn fmt_cmp<T: Debug + Sized>(
                    f: &mut fmt::Formatter,
                    file: &serialized::File,
                    cmp: &CmpType,
                    size: &usize,
                    x: &Reference<()>,
                    y: &Reference<()>,
                    jmp: &Reference<()>,
                ) -> fmt::Result {
                    let x = x.with_type::<T>();
                    let y = y.with_type::<T>();

                    let x = x.printable(file);
                    let y = y.printable(file);

                    write!(f, "cmp (size={size:#x}) {x} {cmp} {y}; jump to: {jmp}")
                }

                // `cmp` is implemented as `memcmp`.  Ordering (Gt, Ge, Lt, Le) is thus *not*
                // numeric order, but string order; therefore, we can only treat the operands as
                // numbers for Eq and Ne.
                let may_be_numeric = matches!(cmp, CmpType::Eq | CmpType::Ne);
                match (size, may_be_numeric) {
                    (1, true) => fmt_cmp::<u8>(f, self.file, cmp, size, x, y, jmp),
                    (2, true) => fmt_cmp::<u16>(f, self.file, cmp, size, x, y, jmp),
                    (4, true) => fmt_cmp::<u32>(f, self.file, cmp, size, x, y, jmp),
                    (8, true) => fmt_cmp::<u64>(f, self.file, cmp, size, x, y, jmp),
                    _ => {
                        write!(f, "cmp (size={size:#x}) {x}")?;
                        if let Ok(x_val) = x.offset.fetch_slice(self.file, *size) {
                            if let Ok(x_str) = std::str::from_utf8(x_val) {
                                write!(f, "={x_str:?}")?;
                            }
                        }
                        write!(f, " {cmp} {y}")?;
                        if let Ok(y_val) = y.offset.fetch_slice(self.file, *size) {
                            if let Ok(y_str) = std::str::from_utf8(y_val) {
                                write!(f, "={y_str:?}")?;
                            }
                        }
                        write!(f, "; jump to: {jmp}")
                    }
                }
            }

            Instruction::ResolveFd {
                fd,
                path,
                typ,
                path_max,
            } => {
                let fd = fd.printable(self.file);
                let path = path.printable(self.file);
                write!(f, "resolvefd {fd} to {typ} -> {path} (path_max={path_max})")
            }
        }
    }
}

impl<'a, 'b, I: Iterator<Item = &'a io::Result<Instruction>>> Iterator
    for PrintableInstructionsIter<'a, 'b, I>
{
    type Item = PrintableInstruction<'a, 'b>;

    fn next(&mut self) -> Option<Self::Item> {
        let instruction = self.insn_iter.next()?;
        if self.skip_multi_end {
            // Ignore errors for toggling `skipping_end` (i.e. when we encounter an invalid
            // instruction in the middle of `End` instructions, we will not reset `skipping_end`,
            // so that the `End` after the invalid instruction continues to be skipped)
            if let Ok(insn) = instruction {
                let end = matches!(insn, Instruction::End);
                if end && self.skipping_end {
                    return self.next();
                }
                self.skipping_end = end;
            }
        }
        Some(PrintableInstruction {
            instruction,
            file: self.file,
        })
    }
}

impl From<&serialized::MetadataType> for MetadataType {
    fn from(s: &serialized::MetadataType) -> Self {
        match s {
            serialized::MetadataType::UidTarget => MetadataType::UidTarget,
            serialized::MetadataType::GidTarget => MetadataType::GidTarget,
            serialized::MetadataType::PidTarget => MetadataType::PidTarget,
        }
    }
}

impl Display for MetadataType {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            MetadataType::UidTarget => write!(f, "target uid"),
            MetadataType::GidTarget => write!(f, "target gid"),
            MetadataType::PidTarget => write!(f, "target pid"),
        }
    }
}

impl TryFrom<&serialized::op::desc::Context> for Context {
    type Error = io::Error;

    fn try_from(s: &serialized::op::desc::Context) -> io::Result<Self> {
        s.type_spec.check_valid()?;

        let spec = &s.type_spec.spec()?;
        let typ = &s.type_spec.typ()?;
        let ctx = match spec {
            serialized::op::desc::ContextSpec::None => {
                return Err(io_error_other("Context is None"))
            }

            serialized::op::desc::ContextSpec::Caller => match typ {
                serialized::op::desc::ContextType::NsMount
                | serialized::op::desc::ContextType::NsCgroup
                | serialized::op::desc::ContextType::NsUts
                | serialized::op::desc::ContextType::NsIpc
                | serialized::op::desc::ContextType::NsUser
                | serialized::op::desc::ContextType::NsPid
                | serialized::op::desc::ContextType::NsNet
                | serialized::op::desc::ContextType::NsTime
                | serialized::op::desc::ContextType::Cwd => {
                    let cs = ContextString::OfCaller;
                    match typ {
                        serialized::op::desc::ContextType::NsMount => Context::NsMount(cs),
                        serialized::op::desc::ContextType::NsCgroup => Context::NsCgroup(cs),
                        serialized::op::desc::ContextType::NsUts => Context::NsUts(cs),
                        serialized::op::desc::ContextType::NsIpc => Context::NsIpc(cs),
                        serialized::op::desc::ContextType::NsUser => Context::NsUser(cs),
                        serialized::op::desc::ContextType::NsPid => Context::NsPid(cs),
                        serialized::op::desc::ContextType::NsNet => Context::NsNet(cs),
                        serialized::op::desc::ContextType::NsTime => Context::NsTime(cs),
                        serialized::op::desc::ContextType::Cwd => Context::Cwd(cs),
                        _ => unreachable!(),
                    }
                }
                serialized::op::desc::ContextType::Uid => Context::Uid(ContextId::OfCaller),
                serialized::op::desc::ContextType::Gid => Context::Gid(ContextId::OfCaller),
            },

            serialized::op::desc::ContextSpec::Num => match typ {
                serialized::op::desc::ContextType::NsMount
                | serialized::op::desc::ContextType::NsCgroup
                | serialized::op::desc::ContextType::NsUts
                | serialized::op::desc::ContextType::NsIpc
                | serialized::op::desc::ContextType::NsUser
                | serialized::op::desc::ContextType::NsPid
                | serialized::op::desc::ContextType::NsNet
                | serialized::op::desc::ContextType::NsTime
                | serialized::op::desc::ContextType::Cwd => {
                    // Safe: Checked spec+type
                    let cs = ContextString::OfPid(
                        unsafe { s.target.pid }.try_into().map_err(io_error_other)?,
                    );
                    match typ {
                        serialized::op::desc::ContextType::NsMount => Context::NsMount(cs),
                        serialized::op::desc::ContextType::NsCgroup => Context::NsCgroup(cs),
                        serialized::op::desc::ContextType::NsUts => Context::NsUts(cs),
                        serialized::op::desc::ContextType::NsIpc => Context::NsIpc(cs),
                        serialized::op::desc::ContextType::NsUser => Context::NsUser(cs),
                        serialized::op::desc::ContextType::NsPid => Context::NsPid(cs),
                        serialized::op::desc::ContextType::NsNet => Context::NsNet(cs),
                        serialized::op::desc::ContextType::NsTime => Context::NsTime(cs),
                        serialized::op::desc::ContextType::Cwd => Context::Cwd(cs),
                        _ => unreachable!(),
                    }
                }
                serialized::op::desc::ContextType::Uid => {
                    // Safe: Checked spec+type
                    Context::Uid(ContextId::Numeric(unsafe { s.target.uid }))
                }
                serialized::op::desc::ContextType::Gid => {
                    // Safe: Checked spec+type
                    Context::Gid(ContextId::Numeric(unsafe { s.target.gid }))
                }
            },

            serialized::op::desc::ContextSpec::Name => match typ {
                serialized::op::desc::ContextType::NsMount
                | serialized::op::desc::ContextType::NsCgroup
                | serialized::op::desc::ContextType::NsUts
                | serialized::op::desc::ContextType::NsIpc
                | serialized::op::desc::ContextType::NsUser
                | serialized::op::desc::ContextType::NsPid
                | serialized::op::desc::ContextType::NsNet
                | serialized::op::desc::ContextType::NsTime
                | serialized::op::desc::ContextType::Cwd => {
                    // Safe: Checked spec+type
                    let s = unsafe { &s.target.path };
                    let s = s.split_once_compat(|c| *c == 0).map(|s| s.0).unwrap_or(s);
                    let s = std::str::from_utf8(s).map_err(io_error_other)?;
                    let cs = ContextString::Fixed(s.to_string());
                    match typ {
                        serialized::op::desc::ContextType::NsMount => Context::NsMount(cs),
                        serialized::op::desc::ContextType::NsCgroup => Context::NsCgroup(cs),
                        serialized::op::desc::ContextType::NsUts => Context::NsUts(cs),
                        serialized::op::desc::ContextType::NsIpc => Context::NsIpc(cs),
                        serialized::op::desc::ContextType::NsUser => Context::NsUser(cs),
                        serialized::op::desc::ContextType::NsPid => Context::NsPid(cs),
                        serialized::op::desc::ContextType::NsNet => Context::NsNet(cs),
                        serialized::op::desc::ContextType::NsTime => Context::NsTime(cs),
                        serialized::op::desc::ContextType::Cwd => Context::Cwd(cs),
                        _ => unreachable!(),
                    }
                }
                serialized::op::desc::ContextType::Uid | serialized::op::desc::ContextType::Gid => {
                    // Safe: Checked spec+type
                    let s = unsafe { &s.target.name };
                    let s = s.split_once_compat(|c| *c == 0).map(|s| s.0).unwrap_or(s);
                    let s = std::str::from_utf8(s).map_err(io_error_other)?;
                    let cid = ContextId::Name(s.to_string());
                    match typ {
                        serialized::op::desc::ContextType::Uid => Context::Uid(cid),
                        serialized::op::desc::ContextType::Gid => Context::Gid(cid),
                        _ => unreachable!(),
                    }
                }
            },
        };

        Ok(ctx)
    }
}

impl Display for Context {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Context::NsMount(s) => write!(f, "ns_mount={s}"),
            Context::NsCgroup(s) => write!(f, "ns_cgroup={s}"),
            Context::NsUts(s) => write!(f, "ns_uts={s}"),
            Context::NsIpc(s) => write!(f, "ns_ipc={s}"),
            Context::NsUser(s) => write!(f, "ns_user={s}"),
            Context::NsPid(s) => write!(f, "ns_pid={s}"),
            Context::NsNet(s) => write!(f, "ns_net={s}"),
            Context::NsTime(s) => write!(f, "ns_time={s}"),
            Context::Cwd(s) => write!(f, "cwd={s}"),
            Context::Uid(id) => write!(f, "uid={id}"),
            Context::Gid(id) => write!(f, "gid={id}"),
        }
    }
}

impl Display for ContextString {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            ContextString::OfCaller => write!(f, "[of caller]"),
            ContextString::OfPid(pid) => write!(f, "[of pid {pid}]"),
            ContextString::Fixed(s) => write!(f, "{s:?}"),
        }
    }
}

impl Display for ContextId {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            ContextId::OfCaller => write!(f, "[of caller]"),
            ContextId::Numeric(id) => write!(f, "{id}"),
            ContextId::Name(name) => write!(f, "{name:?}"),
        }
    }
}

impl From<&serialized::op::desc::ContextSpec> for ContextSpecType {
    fn from(s: &serialized::op::desc::ContextSpec) -> Self {
        use serialized::op::desc::ContextSpec as S;

        match s {
            S::None => ContextSpecType::None,
            S::Caller => ContextSpecType::Caller,
            S::Num => ContextSpecType::Num,
            S::Name => ContextSpecType::Name,
        }
    }
}

impl From<&serialized::op::desc::BitwiseType> for BitwiseType {
    fn from(s: &serialized::op::desc::BitwiseType) -> Self {
        use serialized::op::desc::BitwiseType as S;

        match s {
            S::And => BitwiseType::And,
            S::Or => BitwiseType::Or,
        }
    }
}

impl Display for BitwiseType {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            BitwiseType::And => write!(f, "and"),
            BitwiseType::Or => write!(f, "or"),
        }
    }
}

impl From<&serialized::op::desc::CmpType> for CmpType {
    fn from(s: &serialized::op::desc::CmpType) -> Self {
        use serialized::op::desc::CmpType as S;

        match s {
            S::Eq => CmpType::Eq,
            S::Ne => CmpType::Ne,
            S::Gt => CmpType::Gt,
            S::Ge => CmpType::Ge,
            S::Lt => CmpType::Lt,
            S::Le => CmpType::Le,
        }
    }
}

impl Display for CmpType {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            CmpType::Eq => write!(f, "=="),
            CmpType::Ne => write!(f, "!="),
            CmpType::Gt => write!(f, ">"),
            CmpType::Ge => write!(f, ">="),
            CmpType::Lt => write!(f, "<"),
            CmpType::Le => write!(f, "<="),
        }
    }
}

impl From<&serialized::op::desc::ResolveFdType> for ResolveFdType {
    fn from(s: &serialized::op::desc::ResolveFdType) -> Self {
        use serialized::op::desc::ResolveFdType as S;

        match s {
            S::Path => ResolveFdType::Path,
            S::Mount => ResolveFdType::Mount,
        }
    }
}

impl Display for ResolveFdType {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            ResolveFdType::Path => write!(f, "path"),
            ResolveFdType::Mount => write!(f, "mount"),
        }
    }
}
