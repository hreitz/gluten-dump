use crate::compat::io_error_other;
use crate::gluten::serialized::Offset;
use crate::numerical_enum;
use libc::{c_int, gid_t, pid_t, uid_t, PATH_MAX, _SC_LOGIN_NAME_MAX as LOGIN_NAME_MAX};
use std::io;

#[repr(C)]
pub struct Context {
    pub type_spec: ContextTypeSpec,
    pub target: ContextTarget,
}

#[repr(C)]
pub struct ContextTypeSpec(u8);

numerical_enum! {
    #[derive(Copy, Clone, Debug, Eq, PartialEq)]
    pub enum ContextType as u8/u8 {
        NsMount = 0,
        NsCgroup = 1,
        NsUts = 2,
        NsIpc = 3,
        NsUser = 4,
        NsPid = 5,
        NsNet = 6,
        NsTime = 7,
        Cwd = 8,
        Uid = 9,
        Gid = 10,
    }
}

numerical_enum! {
    #[derive(Copy, Clone, Debug, Eq, PartialEq)]
    pub enum ContextSpec as u8/u8 {
        None = 0,
        Caller = 1,
        Num = 2,
        Name = 3,
    }
}

#[repr(C)]
pub union ContextTarget {
    pub pid: pid_t,
    pub uid: uid_t,
    pub gid: gid_t,
    pub path: [u8; PATH_MAX as usize],
    pub name: [u8; LOGIN_NAME_MAX as usize],
}

#[repr(C)]
pub struct Syscall {
    pub bitfield: SyscallBitfield,
    pub context: Offset,
    // args[]: Offset
}

#[repr(C)]
pub struct SyscallBitfield(u32);

#[repr(C)]
pub struct Fd {
    pub srcfd: Offset,
    pub newfd: Offset,
    pub flags: FdFlags,
}

#[repr(C)]
pub struct FdFlags(u8);

#[repr(C)]
pub struct Return {
    pub val: Offset,
    pub error: Offset,
    pub cont: bool,
}

#[repr(C)]
pub struct Bitwise {
    pub size: usize,
    pub typ: c_int,
    pub dst: Offset,
    pub x: Offset,
    pub y: Offset,
}

numerical_enum! {
    #[derive(Copy, Clone, Debug, Eq, PartialEq)]
    pub enum BitwiseType as C/c_int {
        And = 0,
        Or = 1,
    }
}

#[derive(Debug)]
#[repr(C)]
pub struct Cmp {
    pub cmp: c_int,
    pub size: usize,
    pub x: Offset,
    pub y: Offset,
    pub jmp: Offset,
}

numerical_enum! {
    #[derive(Copy, Clone, Debug, Eq, PartialEq)]
    pub enum CmpType as C/c_int {
        Eq = 0,
        Ne = 1,
        Gt = 2,
        Ge = 3,
        Lt = 4,
        Le = 5,
    }
}

#[repr(C)]
pub struct ResolveFd {
    pub fd: Offset,
    pub path: Offset,
    pub typ: c_int,
    pub path_max: usize,
}

numerical_enum! {
    #[derive(Copy, Clone, Debug, Eq, PartialEq)]
    pub enum ResolveFdType as C/c_int {
        Path = 0,
        Mount = 1,
    }
}

impl ContextTypeSpec {
    pub fn typ(&self) -> io::Result<ContextType> {
        (self.0 & 0xf).try_into()
    }

    pub fn spec(&self) -> io::Result<ContextSpec> {
        ((self.0 >> 4) & 0x3).try_into()
    }

    pub fn check_valid(&self) -> io::Result<()> {
        if self.0 & 0xc0 != 0 {
            Err(io_error_other(format!(
                "Invalid bits in context bitfield set: {:#x}",
                self.0 & 0xc0
            )))
        } else {
            Ok(())
        }
    }
}

impl SyscallBitfield {
    pub fn nr(&self) -> u32 {
        self.0 & 0x1ff
    }

    pub fn arg_count(&self) -> usize {
        ((self.0 >> 9) & 0x7) as usize
    }

    pub fn has_ret(&self) -> bool {
        self.0 & (1 << 12) != 0
    }

    pub fn arg_deref(&self) -> u32 {
        (self.0 >> 13) & 0x3f
    }

    pub fn check_valid(&self) -> io::Result<()> {
        if self.0 & 0xfff8_0000 != 0 {
            Err(io_error_other(format!(
                "Invalid bits in syscall bitfield set: {:#x}",
                self.0 & 0xfff8_0000
            )))
        } else {
            Ok(())
        }
    }
}

impl FdFlags {
    pub fn setfd(&self) -> bool {
        self.0 & (1 << 0) != 0
    }

    pub fn cloexec(&self) -> bool {
        self.0 & (1 << 1) != 0
    }

    pub fn do_return(&self) -> bool {
        self.0 & (1 << 2) != 0
    }

    pub fn check_valid(&self) -> io::Result<()> {
        if self.0 & 0xf8 != 0 {
            Err(io_error_other(format!(
                "Invalid FD flag bits set: {:#x}",
                self.0 & 0xf8
            )))
        } else {
            Ok(())
        }
    }
}

impl Bitwise {
    pub fn typ(&self) -> io::Result<BitwiseType> {
        self.typ.try_into()
    }
}

impl Cmp {
    pub fn cmp(&self) -> io::Result<CmpType> {
        self.cmp.try_into()
    }
}

impl ResolveFd {
    pub fn typ(&self) -> io::Result<ResolveFdType> {
        self.typ.try_into()
    }
}
