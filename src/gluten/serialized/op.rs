use crate::gluten::serialized::Offset;
use crate::numerical_enum;
use libc::c_int;
use std::io;
use std::mem::ManuallyDrop;

pub mod desc;

numerical_enum! {
    #[derive(Copy, Clone, Debug, Default, Eq, PartialEq)]
    pub enum Type as C/c_int {
        #[default]
        End = 0,
        Nr = 1,
        Call = 2,
        Copy = 3,
        Fd = 4,
        FdGet = 5,
        Return = 6,
        Load = 7,
        Store = 8,
        IovLoad = 9,
        Bitwise = 10,
        Cmp = 11,
        ResolveFd = 12,
    }
}

#[repr(C)]
pub struct Op {
    pub typ: c_int,
    pub op: OpParam,
}

#[repr(C)]
pub union OpParam {
    // Rust requires `union` members to implement `Copy` or be `ManuallyDrop`.  These are just
    // serialized objects, i.e. basically only data containers, so there is nothing to be done when
    // dropping them and wrapping them like so is fine.
    pub nr: ManuallyDrop<Nr>,
    pub call: ManuallyDrop<Call>,
    pub copy: ManuallyDrop<Copy>,
    pub fd: ManuallyDrop<Fd>,
    pub fdget: ManuallyDrop<FdGet>,
    pub ret: ManuallyDrop<Return>,
    pub load: ManuallyDrop<Load>,
    pub store: ManuallyDrop<Store>,
    pub iovload: ManuallyDrop<IovLoad>,
    pub bitwise: ManuallyDrop<Bitwise>,
    pub cmp: ManuallyDrop<Cmp>,
    pub resfd: ManuallyDrop<ResolveFd>,
}

#[repr(C)]
pub struct Nr {
    pub nr: Offset,
    pub no_match: Offset,
}

#[repr(C)]
pub struct Call {
    pub desc: Offset,
}

#[repr(C)]
pub struct Copy {
    pub src: Offset,
    pub dst: Offset,
    pub size: usize,
}

#[repr(C)]
pub struct Fd {
    pub desc: Offset,
}

#[repr(C)]
pub struct FdGet {
    pub src: Offset,
    pub dst: Offset,
}

#[repr(C)]
pub struct Return {
    pub desc: Offset,
}

#[repr(C)]
pub struct Load {
    pub src: Offset,
    pub dst: Offset,
    pub size: usize,
}

#[repr(C)]
pub struct Store {
    pub src: Offset,
    pub dst: Offset,
    pub count: Offset,
}

#[repr(C)]
pub struct IovLoad {
    pub iov: Offset,
    pub iovlen: Offset,
    pub dst: Offset,
    pub size: usize,
}

#[repr(C)]
pub struct Bitwise {
    pub desc: Offset,
}

#[repr(C)]
pub struct Cmp {
    pub desc: Offset,
}

#[repr(C)]
pub struct ResolveFd {
    pub desc: Offset,
}

impl Op {
    pub fn typ(&self) -> io::Result<Type> {
        self.typ.try_into()
    }
}
