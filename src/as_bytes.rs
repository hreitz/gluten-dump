use crate::compat::NewUninitCompat;
use std::io::{self, Read};
use std::mem::{size_of, MaybeUninit};

pub trait AsBytes {
    #[allow(dead_code)]
    fn as_bytes(&self) -> &[u8];
}

pub trait AsBytesMut {
    unsafe fn as_bytes_mut(&mut self) -> &mut [u8];
}

pub trait ReadObject {
    type Error;

    unsafe fn read_boxed_object<T: AsBytesMut>(&mut self) -> Result<Box<T>, Self::Error>;
}

impl<T: Sized> AsBytes for T {
    fn as_bytes(&self) -> &[u8] {
        let ptr: *const T = self;
        let size = size_of::<T>();
        // Safe: Memory address and size are valid
        unsafe { std::slice::from_raw_parts(ptr.cast::<u8>(), size) }
    }
}

impl<T: Sized> AsBytesMut for T {
    /// Safety: Converting arbitrary data must always result in a valid object of type `T`
    unsafe fn as_bytes_mut(&mut self) -> &mut [u8] {
        let ptr: *mut T = self;
        let size = size_of::<T>();
        // Safe: Memory address and size are valid; and our caller guarantees that modifying the
        // contents is safe
        unsafe { std::slice::from_raw_parts_mut(ptr.cast::<u8>(), size) }
    }
}

impl<R: Read> ReadObject for R {
    type Error = io::Error;

    /// Safety: Converting arbitrary data must always result in a valid object of type `T`
    unsafe fn read_boxed_object<T: AsBytesMut>(&mut self) -> io::Result<Box<T>> {
        let mut obj: Box<MaybeUninit<T>> = Box::new_uninit_compat();
        // Safe: Caller guarantees this is safe
        self.read_exact(unsafe { AsBytesMut::as_bytes_mut(&mut *obj) })?;
        // Safe: `read_exact()` guarantees everything has been written
        Ok(unsafe { obj.assume_init_compat() })
    }
}
