use std::fmt::Display;
use std::io;

pub trait ErrorContext {
    fn context<C: Display>(self, context: C) -> Self;
}

impl ErrorContext for io::Error {
    fn context<C: Display>(self, context: C) -> Self {
        io::Error::new(self.kind(), format!("{context}: {self}"))
    }
}

pub trait ResultErrorContext {
    fn err_context<C: Display, F: FnOnce() -> C>(self, context: F) -> Self;
}

impl<V, E: ErrorContext> ResultErrorContext for Result<V, E> {
    fn err_context<C: Display, F: FnOnce() -> C>(self, context: F) -> Self {
        self.map_err(|err| err.context(context()))
    }
}
